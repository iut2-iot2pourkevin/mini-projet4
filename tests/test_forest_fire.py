import unittest
import propage

class Testforest_fire(unittest.TestCase):
    
    def test_list_verify(self):
        the_list = ['FIRE','TREE','EMPTY']

        self.assertEqual(propage.list_verify('ASHES',the_list),False)
        self.assertEqual(propage.list_verify('FIRE',the_list),True)
        self.assertEqual(propage.list_verify('Fire',the_list),False)