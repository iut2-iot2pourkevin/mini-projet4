from states import *

"""
Verify if element is an element in the_list
"""
def list_verify(element,the_list):
    flag = False
    for iter in the_list:
        if (iter == element):
            flag = True
    return flag

def propager_nord(grid,index1,index2,list_feux):
    try : 
        if(grid[index1-1][index2] == TREE and list_verify([index1,index2],list_feux)):
            grid[index1-1][index2] = FIRE
    except IndexError:
        pass
    return grid

def propager_sud(grid,index1,index2,list_feux):
    try:
        if(grid[index1+1][index2] == TREE and list_verify([index1,index2],list_feux)):
            grid[index1+1][index2] = FIRE
    except IndexError:
        pass
    return grid

def propager_ouest(grid,index1,index2,list_feux):
    try:
        if(grid[index1][index2-1] == TREE and list_verify([index1,index2],list_feux)):
            grid[index1][index2-1] = FIRE
    except IndexError:
        pass
    return grid

def propager_est(grid,index1,index2,list_feux):
    try:
        if(grid[index1][index2+1] == TREE and list_verify([index1,index2],list_feux)):
            grid[index1][index2+1] = FIRE
    except IndexError:
        pass
    return grid
