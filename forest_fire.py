#region Imports
import argparse
import tkinter
import tkinter.messagebox
import random
from states import *
import datetime
import propage

#endregion

#region Functions

    #region Init and Grid display
"""
IN : aCanvas (Canvas), nbrows (int), nbcols (int), cell_size (int), afforestation(0<float<1)
OUT : grid (Tableau à  deux dimensions [nbrows][nbcols]) d'entiers(représentants un type de sol)
Remplie le tableau grid avec des 0 (présence d'arbres) ou des 3 (vide) selon le taux d'afforestation
"""
def init_grid(grid,nbrows, nbcols, cell_size, afforestation):
    grid.clear()
    for iter1 in range(nbrows):
        columnnum = []
        for iter2 in range(nbcols):
            if(afforestation > random.random()):
                state = TREE
            else :
                state = EMPTY
            columnnum.append(state)
        grid.append(columnnum)
    afficher_carte()
    return grid

"""
IN : grid (tableau d'entiers)
OUT : affichage graphique sur Canvas
"""
def afficher_carte():
    unCanvas.delete("all")
    for iter1 in range(nbrows):
        for iter2 in range (nbcols):
            if(grid[iter1][iter2] == TREE):
                color = "#476042"
            elif(grid[iter1][iter2] == FIRE):
                color = "red"
            elif(grid[iter1][iter2] == ASHES):
                color = "grey"
            else:
                color = "white"
            unCanvas.create_rectangle(iter2*cell_size,iter1*cell_size,(iter2+1)*cell_size,(iter1+1)*cell_size,fill=color)

    #endregion

    #region EventsHandler
"""
IN : event (mouse click)
OUT : grid (un élément modifié, correspondant au rectangle cliqué), feu ajouté
"""
def set_on_fire_callback(event):
    index1 = int(event.x/cell_size)
    index2 = int(event.y/cell_size)
    if(grid[index2][index1] == TREE):
        grid[index2][index1] = FIRE
        afficher_carte()

"""
IN : event (mouse click)
OUT : grid (un élément modifié, correspondant au rectangle cliqué), feu retiré
"""
def let_him_live_callback(event):
    index1 = int(event.x/cell_size)
    index2 = int(event.y/cell_size)
    if(grid[index2][index1] == FIRE):
        grid[index2][index1] = TREE
        afficher_carte()
    #endregion

    #region Animations

"""
IN: grid (tableau d'entiers), duration (durée en secondes)
OUT: Afichage graphique
OBJECTIF: Afficher une animation du feu, des cendres et des arbres sur frame(fenetre)
"""
def launch_animation(grid,duration):
    positions_feux = recenser_feux(grid)
    position_ashes = []
    for i in range(nbrows):
        for j in range(nbcols):
            if(grid[i][j] == FIRE):
                grid = propager_regle(grid,i,j,positions_feux)
            elif(grid[i][j] == ASHES):
                position_ashes.append([i,j])
    if(positions_feux != []):
        transform_fire_and_ashes(positions_feux,position_ashes)
        afficher_carte()
        frame.after(int(duration * 1000),lambda:launch_animation(grid,duration))
    else:
        transform_fire_and_ashes(positions_feux,position_ashes)
        afficher_carte()

"""
IN: grid (tableau d'entiers)
OUT: List_feux (tableaux de coordonnées [x,y] des feux)
OBJECTIF: Enregistrer les positions des feux avant de propager
"""
def recenser_feux(grid):
    list_feux = []
    for i in range(nbrows):
        for j in range(nbcols):
            if (grid[i][j] == FIRE):
                list_feux.append([i,j])
    return list_feux

"""
IN: grid (tableau d'entiers), index1 et index2 (entiers), list_feux(liste de coordonnées[x,y])
OUT: grid modifié
OBJECTIF: Propager les feux autour des cases en feu au début du tour
"""
def propager_regle(grid,index1,index2,list_feux):
    if (rule2_checked.get() == 1):
        #code for rule 2 needed
        pass
    else:
        grid = propage.propager_nord(grid,index1,index2,list_feux)
        grid = propage.propager_sud(grid,index1,index2,list_feux)
        grid = propage.propager_ouest(grid,index1,index2,list_feux)
        grid = propage.propager_est(grid,index1,index2,list_feux)
    return grid

"""
IN: fire_list et ashes_list (listes de coordonnées[x,y])
OUT: List_feux (tableaux de coordonnées [x,y] des feux)
OBJECTIF: Transformer les feux en cendres et les cendres en case vide
"""
def transform_fire_and_ashes(fire_list,ashes_list):
    for afire in fire_list:
        grid[afire[0]][afire[1]] = grid[afire[0]][afire[1]] + 1
    for anash in ashes_list:
        grid[anash[0]][anash[1]] = grid[anash[0]][anash[1]] + 1

    #endregion

#endregion

#region Main Program

"""""""""
### PARAMETERS ###
"""""""""

parser = argparse.ArgumentParser()
parser.add_argument('-cols',help='number of columns')
parser.add_argument('-rows',help='number of rows')
parser.add_argument('-animation',help='number of columns')
parser.add_argument('-cell_size',help='number of columns')
parser.add_argument('-afforestation',help='number of columns')

args = parser.parse_args()

if args.rows:
    nbrows = int(args.rows)
else:
    nbrows = 20
if args.cols:
    nbcols = int(args.cols)
else:
    nbcols = 30
if args.animation:
    duration = float(args.animation)
else:
    duration = .5
if args.cell_size:
    cell_size = int(args.cell_size)
else:
    cell_size = 40
if args.afforestation:
    afforestation = float(args.afforestation)
else:
    afforestation = .6

"""""""""
### VARIABLES ###
"""""""""
grid = []
# Calculating variables value
canvas_width = cell_size*nbcols
canvas_height = cell_size*nbrows

"""""""""
### TKINTER ###
"""""""""
# Setting the frame and Buttons
frame = tkinter.Tk()

rule2_checked = tkinter.IntVar()

startButton = tkinter.Button(frame, text="Lancer/ Continuer l'animation", command=lambda:launch_animation(grid,duration))
restarButton = tkinter.Button(frame, text="changer le terrain", command=lambda:init_grid(grid,nbrows,nbcols,cell_size,afforestation))
rule2_checkbox = tkinter.Checkbutton(frame, text = "activez la regle 2", variable = rule2_checked)

# Setting Canvas and displaying grid on it
unCanvas = tkinter.Canvas(frame, width=canvas_width, height=canvas_height)
grid = init_grid(grid,nbrows,nbcols,cell_size,afforestation)
unCanvas.pack()
rule2_checkbox.pack()
startButton.pack()
restarButton.pack()
unCanvas.bind('<Button-1>', set_on_fire_callback)
unCanvas.bind('<Button-3>', let_him_live_callback)
afficher_carte()

#starting frame
frame.mainloop()

#endregion